﻿
using BusinessLogicFizzBuzz.InterfacesFizzBuzz;

namespace BusinessLogicFizzBuzz.ClassesFizzBuzz
{
    public class ReturnResultThree : IReturnResultThree
    {
        private readonly ICheckDivisibilityByThree result;
        public ReturnResultThree(ICheckDivisibilityByThree result)
        {
            this.result = result;
        }
        public string ResultThree(int userinput)
        {
            if (this.result.CheckIfDivisibleByThree(userinput))
            {
                return "fizz";
            }
            else
            {
                return null;
            }
        }
    }
}
