﻿
using System.Collections.Generic;
using BusinessLogicFizzBuzz.InterfacesFizzBuzz;

namespace BusinessLogicFizzBuzz.ClassesFizzBuzz
{
    public class NullRemover : INullRemover
    {
        public List<string> Remover (List<string> listofstrings)
        {
            List<string> nonulllist = new List<string>();
            foreach (var element in listofstrings)
            {
                if (element != null)
                {
                    nonulllist.Add(element);
                }
            }
            return nonulllist;
        }
    }
}
