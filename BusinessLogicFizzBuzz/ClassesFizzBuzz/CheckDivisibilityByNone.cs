﻿using BusinessLogicFizzBuzz.InterfacesFizzBuzz;


namespace BusinessLogicFizzBuzz.ClassesFizzBuzz
{
    public class CheckDivisibilityByNone : ICheckDivisibilityByNone
    {
        public bool CheckIfDivisibleByNone(int userinput)
        {
            if (userinput %3 != 0 && userinput % 5 != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
