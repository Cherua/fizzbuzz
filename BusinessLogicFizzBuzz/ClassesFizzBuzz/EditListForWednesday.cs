﻿using System;
using System.Collections.Generic;
using BusinessLogicFizzBuzz.InterfacesFizzBuzz;

namespace BusinessLogicFizzBuzz.ClassesFizzBuzz
{
    public class EditListForWednesday : IEditListForWednesday
    {
        private readonly ICheckDayOfWeek daytoday;
        public EditListForWednesday(ICheckDayOfWeek daytoday)
        {
            this.daytoday = daytoday;
        }
        public List<string> CheckDayOfWeek(List<string> nonulllist)
        {
            List<string> finallist = new List<string>();
            foreach (var element in nonulllist)
            {
                if (this.daytoday.CheckDayToday(DateTime.Today.DayOfWeek))
                {
                    if (element.Equals("fizz",StringComparison.OrdinalIgnoreCase))
                    {
                        finallist.Add("wizz");
                    }
                    else if (element.Equals("buzz", StringComparison.OrdinalIgnoreCase))
                    {
                        finallist.Add("wuzz");
                    }
                    else if (element.Equals("fizzbuzz", StringComparison.OrdinalIgnoreCase))
                    {
                        finallist.Add("wizzwuzz");
                    }
                    else
                    {
                        finallist.Add(element);
                    }
                }
                else
                {
                    finallist.Add(element);
                }
            }
            return finallist;

        }
    }
}
