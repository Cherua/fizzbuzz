﻿using BusinessLogicFizzBuzz.InterfacesFizzBuzz;

namespace BusinessLogicFizzBuzz.ClassesFizzBuzz
{
    public class CheckDivisibilityByThree : ICheckDivisibilityByThree
    {
        public bool CheckIfDivisibleByThree(int userinput)
        {
            if (userinput % 3 == 0 && userinput % 5 != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
