﻿using System;
using BusinessLogicFizzBuzz.InterfacesFizzBuzz;

namespace BusinessLogicFizzBuzz.ClassesFizzBuzz
{
    public class CheckDayOfWeek : ICheckDayOfWeek
    {
        public bool CheckDayToday(DayOfWeek day)
        {
            return day == DayOfWeek.Wednesday;
        }
    }
}
