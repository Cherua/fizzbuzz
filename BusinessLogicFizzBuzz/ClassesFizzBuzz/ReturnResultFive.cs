﻿
using BusinessLogicFizzBuzz.InterfacesFizzBuzz;

namespace BusinessLogicFizzBuzz.ClassesFizzBuzz
{
    public class ReturnResultFive : IReturnResultFive
    {
        private readonly ICheckDivisibilityByFive result;
        public ReturnResultFive(ICheckDivisibilityByFive result)
        {
            this.result = result;
        }
        public string ResultFive(int userinput)
        {
            if (this.result.CheckIfDivisibleByFive(userinput))
            {
                return "buzz";
            }
            else
            {
                return null;
            }
        }
    }
}
