﻿using BusinessLogicFizzBuzz.InterfacesFizzBuzz;

namespace BusinessLogicFizzBuzz.ClassesFizzBuzz
{
    public class CheckDivisibilityByFive : ICheckDivisibilityByFive
    {
        public bool CheckIfDivisibleByFive(int userinput)
        {
            if (userinput % 5 == 0 && userinput % 3 != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
