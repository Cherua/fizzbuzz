﻿using System.Collections.Generic;
using BusinessLogicFizzBuzz.InterfacesFizzBuzz;

namespace FizzBuzz.Controllers
{
    public class LogicRegulator : ILogicRegulator
    {
        private readonly IReturnResultFifteen resultfifteen;
        private readonly IReturnResultThree resultthree;
        private readonly IReturnResultFive resultfive;
        private readonly IReturnResultNone resultnone;
        private readonly INullRemover remover;
        private readonly IEditListForWednesday wednesdaylist;
        public LogicRegulator(IReturnResultFifteen resultfifteen, IReturnResultThree resultthree, IReturnResultFive resultfive,
             IReturnResultNone resultnone, INullRemover remover, IEditListForWednesday wednesdaylist)
            
        {
            this.resultfifteen = resultfifteen;
            this.resultthree = resultthree;
            this.resultfive = resultfive;
            this.resultnone = resultnone; 
            
            this.remover = remover;
            this.wednesdaylist = wednesdaylist;
        }
        readonly List<string> finallist = new List<string>();
        List<string> finallist_nonull = new List<string>();
        List<string> finallist_nonull_dayofweek = new List<string>();
        public List<string> OutputList(int userinput)
        {
            for (int initial = 1; initial <= userinput; initial++)
            {
                finallist.Add(this.resultfifteen.ResultFifteen(initial));
                finallist.Add(this.resultfive.ResultFive(initial));
                finallist.Add(this.resultthree.ResultThree(initial));
                finallist.Add(this.resultnone.ResultNone(initial));
                
            }
            finallist_nonull = this.remover.Remover(finallist);
            finallist_nonull_dayofweek = this.wednesdaylist.CheckDayOfWeek(finallist_nonull);
            return finallist_nonull_dayofweek;
        }
    }
}