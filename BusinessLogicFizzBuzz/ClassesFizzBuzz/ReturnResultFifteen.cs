﻿using BusinessLogicFizzBuzz.InterfacesFizzBuzz;

namespace BusinessLogicFizzBuzz.ClassesFizzBuzz
{
    public class ReturnResultFifteen : IReturnResultFifteen
    {
        private readonly ICheckDivisibilityByFifteen result;
        public ReturnResultFifteen(ICheckDivisibilityByFifteen result)
        {
            this.result = result;
        }
        public string ResultFifteen(int userinput)
        {
            if (this.result.CheckIfDivisibleByFifteen(userinput))
            {
                return "fizzbuzz";
            }
            else
            {
                return null;
            }
        }
    }
}
