﻿
using BusinessLogicFizzBuzz.InterfacesFizzBuzz;

namespace BusinessLogicFizzBuzz.ClassesFizzBuzz
{
    public class ReturnResultNone : IReturnResultNone
    {
        private readonly ICheckDivisibilityByNone result;
        public ReturnResultNone(ICheckDivisibilityByNone result)
        {
            this.result = result;
        }
        public string ResultNone(int userinput)
        {
            if (this.result.CheckIfDivisibleByNone(userinput))
            {
                return userinput.ToString();
            }
            else
            {
                return null;
            }
        }
    }
}
