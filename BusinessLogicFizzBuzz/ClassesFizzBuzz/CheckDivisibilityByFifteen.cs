﻿using BusinessLogicFizzBuzz.InterfacesFizzBuzz;

namespace BusinessLogicFizzBuzz.ClassesFizzBuzz
{
    public class CheckDivisibilityByFifteen : ICheckDivisibilityByFifteen
    {
        public bool CheckIfDivisibleByFifteen(int userinput)
        {
            if (userinput % 3 == 0 && userinput % 5 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
