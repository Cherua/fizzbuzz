﻿

namespace BusinessLogicFizzBuzz.InterfacesFizzBuzz
{
    public interface ICheckDivisibilityByNone
    {
        bool CheckIfDivisibleByNone(int userinput);
    }
}
