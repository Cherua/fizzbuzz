﻿
using System.Collections.Generic;


namespace BusinessLogicFizzBuzz.InterfacesFizzBuzz
{
    public interface IEditListForWednesday
    {
        List<string> CheckDayOfWeek(List<string> nonulllist);
    }
}
