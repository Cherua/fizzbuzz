﻿

namespace BusinessLogicFizzBuzz.InterfacesFizzBuzz
{
    public interface ICheckDivisibilityByFifteen
    {
        bool CheckIfDivisibleByFifteen(int userinput);
    }
}
