﻿using System;


namespace BusinessLogicFizzBuzz.InterfacesFizzBuzz
{
    public interface ICheckDayOfWeek
    {
        bool CheckDayToday(DayOfWeek day);
    }
}
