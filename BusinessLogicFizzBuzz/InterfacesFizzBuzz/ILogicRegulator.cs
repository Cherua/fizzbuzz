﻿
using System.Collections.Generic;


namespace FizzBuzz.Controllers
{
    public interface ILogicRegulator
    {
        List<string> OutputList(int userinput);
    }
}
