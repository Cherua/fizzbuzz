﻿using System.Collections.Generic;


namespace BusinessLogicFizzBuzz.InterfacesFizzBuzz
{
    public interface INullRemover
    {
        List<string> Remover(List<string> listofstrings);
    }
}
