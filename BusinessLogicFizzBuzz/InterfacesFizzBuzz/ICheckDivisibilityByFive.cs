﻿

namespace BusinessLogicFizzBuzz.InterfacesFizzBuzz
{
    public interface ICheckDivisibilityByFive
    {
        bool CheckIfDivisibleByFive(int userinput);
    }
}
