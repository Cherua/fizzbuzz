﻿

namespace BusinessLogicFizzBuzz.InterfacesFizzBuzz
{
    public interface ICheckDivisibilityByThree
    {
        bool CheckIfDivisibleByThree(int userinput);
    }
}
