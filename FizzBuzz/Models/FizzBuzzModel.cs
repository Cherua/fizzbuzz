﻿
using System.ComponentModel.DataAnnotations;

namespace FizzBuzz.Models
{
    public class FizzBuzzModel
    {
        [Required(ErrorMessage = "Please enter a value")]
        [Range(1, 1000,ErrorMessage = "Please enter a value within the range 1 - 1000")]
        public int userinput { get; set; }
    }
}