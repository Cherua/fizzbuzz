﻿
using System.Collections.Generic;
using System.Linq;

using System.Web.Mvc;
using FizzBuzz.Models;
using PagedList;


namespace FizzBuzz.Controllers
{
    public class FizzBuzzController : Controller
    {

        private readonly ILogicRegulator regulator;
        
        public FizzBuzzController(ILogicRegulator regulator)
        {
            this.regulator = regulator;
        }
        // GET: FizzBuzz
        [HttpGet]
        public ActionResult Index()
        {
            return View(new FizzBuzzModel());
        }
        [HttpPost]
        public ActionResult Result(FizzBuzzModel viewobject)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index", "FizzBuzz");
            }
            var finaloutput = this.regulator.OutputList(viewobject.userinput);
            TempData["outputlist"] = finaloutput;
            return RedirectToAction("Output", "FizzBuzz");

        } 
        public ActionResult Output(int? page)
        {
            
            List<string> listofpages = (List<string>)TempData.Peek("outputlist");
            return View(listofpages.ToList().ToPagedList(page ?? 1,20));
        }
    }
}