﻿

using NUnit;
using NUnit.Framework;

using BusinessLogicFizzBuzz.InterfacesFizzBuzz;
using BusinessLogicFizzBuzz.ClassesFizzBuzz;



namespace UnitTestingFizzBuzz
{
    [TestFixture]
    public class TestDivisibiltyThree
    {
        ICheckDivisibilityByThree fizz;
        [SetUp]
        public void LoadContext()
        {
            fizz = new CheckDivisibilityByThree();
        }
        [TestCase]
        public void passes_if_divisibleby_three()
        {
            Assert.AreEqual(true, fizz.CheckIfDivisibleByThree(3));            
        }
        [TestCase]
        public void passes_if_not_divisibleby_three()
        {
            Assert.AreEqual(false, fizz.CheckIfDivisibleByThree(4));
        }

    }
}
