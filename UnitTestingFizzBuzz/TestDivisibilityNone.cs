﻿

using NUnit;
using NUnit.Framework;

using BusinessLogicFizzBuzz.InterfacesFizzBuzz;
using BusinessLogicFizzBuzz.ClassesFizzBuzz;



namespace UnitTestingFizzBuzz
{
    [TestFixture]
    public class TestDivisibiltyNone
    {
        ICheckDivisibilityByNone none;
        [SetUp]
        public void LoadContext()
        {
            none = new CheckDivisibilityByNone();
        }
        [TestCase]
        public void passes_if_divisibleby_none()
        {
            Assert.AreEqual(true, none.CheckIfDivisibleByNone(23));
        }
        [TestCase]
        public void passes_if_not_divisibleby_none()
        {
            Assert.AreEqual(false, none.CheckIfDivisibleByNone(15));
        }

    }
}
