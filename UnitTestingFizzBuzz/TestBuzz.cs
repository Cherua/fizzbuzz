﻿

using NUnit;
using NUnit.Framework;

using BusinessLogicFizzBuzz.InterfacesFizzBuzz;
using BusinessLogicFizzBuzz.ClassesFizzBuzz;

using Moq;



namespace UnitTestingFizzBuzz
{
    [TestFixture]
    public class TestBuzz
    {
        IReturnResultFive result;
        Mock<ICheckDivisibilityByFive> value;

        [SetUp]
        public void LoadContext()
        {
            value = new Mock<ICheckDivisibilityByFive>();
            result = new ReturnResultFive(value.Object);
        }
        [TestCase]
        public void passes_fizzbuzz_string_if_divisible_by_five()
        {
            value.Setup(x => x.CheckIfDivisibleByFive(15)).Returns(true);
            Assert.AreEqual("buzz", result.ResultFive(15));
        }
        [TestCase]
        public void passes_if_not_divisibleby_five()
        {
            value.Setup(x => x.CheckIfDivisibleByFive(21)).Returns(false);
            Assert.AreEqual(null, result.ResultFive(21));
        }

    }
}
