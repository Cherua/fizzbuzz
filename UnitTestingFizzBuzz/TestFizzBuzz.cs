﻿

using NUnit;
using NUnit.Framework;

using BusinessLogicFizzBuzz.InterfacesFizzBuzz;
using BusinessLogicFizzBuzz.ClassesFizzBuzz;

using Moq;



namespace UnitTestingFizzBuzz
{
    [TestFixture]
    public class TestFizzBuzz
    {
        IReturnResultFifteen result;
        Mock<ICheckDivisibilityByFifteen> value;

        [SetUp]
        public void LoadContext()
        {
            value = new Mock<ICheckDivisibilityByFifteen>();
            result = new ReturnResultFifteen(value.Object);
        }
        [TestCase]
        public void passes_fizzbuzz_string_if_divisible_by_fifteen()
        {
            value.Setup(x => x.CheckIfDivisibleByFifteen(15)).Returns(true);
            Assert.AreEqual("fizzbuzz", result.ResultFifteen(15));
        }
        [TestCase]
        public void passes_if_not_divisibleby_fifteen()
        {
            value.Setup(x => x.CheckIfDivisibleByFifteen(21)).Returns(false);
            Assert.AreEqual(null, result.ResultFifteen(21));
        }

    }
}
