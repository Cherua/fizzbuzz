﻿using System;
using System.Collections.Generic;


using NUnit;
using NUnit.Framework;
using Moq;

using BusinessLogicFizzBuzz.InterfacesFizzBuzz;
using BusinessLogicFizzBuzz.ClassesFizzBuzz;


namespace UnitTestingFizzBuzz
{
    [TestFixture]
    public class TestDayOfWeek
    {
        Mock<ICheckDayOfWeek> daycheck;
        IEditListForWednesday values;

        
        List<string> normallist = new List<string>(new string[] {"fizz", "buzz", "fizzbuzz", "1"});
        List<string> wednesdaylist = new List<string>(new string[] { "wizz", "wuzz", "wizzwuzz", "1"});

        [SetUp]
        public void LoadContext()
        {
            daycheck = new Mock<ICheckDayOfWeek>();
            values = new EditListForWednesday(daycheck.Object);
        }
        [TestCase]
        public void ReturnWizzWuzz()
        {
            daycheck.Setup(x => x.CheckDayToday(DateTime.Today.DayOfWeek)).Returns(true);
            Assert.AreEqual(values.CheckDayOfWeek(normallist),wednesdaylist);
        }
        [TestCase]
        public void ReturnFizzbuzz()
        {
            daycheck.Setup(x => x.CheckDayToday(DateTime.Today.DayOfWeek)).Returns(false);
            Assert.AreEqual(values.CheckDayOfWeek(normallist), normallist);
        }
    }
}
