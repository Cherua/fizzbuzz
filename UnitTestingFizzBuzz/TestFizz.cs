﻿

using NUnit;
using NUnit.Framework;

using BusinessLogicFizzBuzz.InterfacesFizzBuzz;
using BusinessLogicFizzBuzz.ClassesFizzBuzz;

using Moq;



namespace UnitTestingFizzBuzz
{
    [TestFixture]
    public class TestFizz
    {
        IReturnResultThree result;
        Mock<ICheckDivisibilityByThree> value;

        [SetUp]
        public void LoadContext()
        {
            value = new Mock<ICheckDivisibilityByThree>();
            result = new ReturnResultThree(value.Object);
        }
        [TestCase]
        public void passes_fizzbuzz_string_if_divisible_by_three()
        {
            value.Setup(x => x.CheckIfDivisibleByThree(15)).Returns(true);
            Assert.AreEqual("fizz", result.ResultThree(15));
        }
        [TestCase]
        public void passes_if_not_divisibleby_three()
        {
            value.Setup(x => x.CheckIfDivisibleByThree(23)).Returns(false);
            Assert.AreEqual(null, result.ResultThree(23));
        }

    }
}
