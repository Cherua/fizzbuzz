﻿

using NUnit;
using NUnit.Framework;

using BusinessLogicFizzBuzz.InterfacesFizzBuzz;
using BusinessLogicFizzBuzz.ClassesFizzBuzz;



namespace UnitTestingFizzBuzz
{
    [TestFixture]
    public class TestDivisibiltyFifteen
    {
        ICheckDivisibilityByFifteen fizzbuzz;
        [SetUp]
        public void LoadContext()
        {
            fizzbuzz = new CheckDivisibilityByFifteen();
        }
        [TestCase]
        public void passes_if_divisibleby_fifteen()
        {
            Assert.AreEqual(true, fizzbuzz.CheckIfDivisibleByFifteen(15));
        }
        [TestCase]
        public void passes_if_not_divisibleby_fifteen()
        {
            Assert.AreEqual(false, fizzbuzz.CheckIfDivisibleByFifteen(21));
        }

    }
}
