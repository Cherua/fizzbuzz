﻿
using NUnit;
using NUnit.Framework;

using BusinessLogicFizzBuzz.InterfacesFizzBuzz;
using BusinessLogicFizzBuzz.ClassesFizzBuzz;

using Moq;



namespace UnitTestingFizzBuzz
{
    [TestFixture]
    public class TestNumber
    {
        IReturnResultNone result;
        Mock<ICheckDivisibilityByNone> value;

        [SetUp]
        public void LoadContext()
        {
            value = new Mock<ICheckDivisibilityByNone>();
            result = new ReturnResultNone(value.Object);
        }
        [TestCase]
        public void passes_fizzbuzz_string_if_divisible_by_none()
        {
            value.Setup(x => x.CheckIfDivisibleByNone(23)).Returns(true);
            Assert.AreEqual("23", result.ResultNone(23));
        }
        [TestCase]
        public void passes_if_not_divisibleby_five()
        {
            value.Setup(x => x.CheckIfDivisibleByNone(15)).Returns(false);
            Assert.AreEqual(null, result.ResultNone(15));
        }

    }
}
