﻿

using NUnit;
using NUnit.Framework;

using BusinessLogicFizzBuzz.InterfacesFizzBuzz;
using BusinessLogicFizzBuzz.ClassesFizzBuzz;



namespace UnitTestingFizzBuzz
{
    [TestFixture]
    public class TestDivisibiltyFive
    {
        ICheckDivisibilityByFive buzz;
        [SetUp]
        public void LoadContext()
        {
            buzz = new CheckDivisibilityByFive();
        }
        [TestCase]
        public void passes_if_divisibleby_five()
        {
            Assert.AreEqual(true, buzz.CheckIfDivisibleByFive(5));
        }
        [TestCase]
        public void passes_if_not_divisibleby_five()
        {
            Assert.AreEqual(false, buzz.CheckIfDivisibleByFive(6));
        }

    }
}
